package com.company.classes;

import com.company.interfaces.StartInterface;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Start implements StartInterface {

    public void reader() throws IOException {
        String text = new String(Files.readAllBytes(Paths.get("\\Collections\\src\\com\\company\\file.txt")));
        splitString(text);
    }

    public void splitString(String string) {
        String strLower = string.toLowerCase();
        String delims = "[ .,?!':;«»—-]+";
        String[] tokens = strLower.split(delims);
        start(tokens);
    }

    public void start(String[] strings) {
        Methods methods = new Methods();
        methods.numberOfRepeat(strings);
        methods.uniqueWords(strings);
        methods.sortList(strings);
    }
}
