package com.company.classes;

import com.company.interfaces.MethodsInterface;

import java.util.*;

public class Methods implements MethodsInterface {


    public void numberOfRepeat(String[] strings) {
        Map<String, Long> sorted = new HashMap<>();
        for (String string : strings) {
            if (!sorted.containsKey(string)) {
                sorted.put(string, (long) 1);
            } else sorted.put(string, sorted.get(string) + 1);
        }

        System.out.println("Число повторений:");

        for (Map.Entry<String, Long> map : sorted.entrySet()) {
            String a = map.getKey();
            Long b = map.getValue();
            System.out.print(a + " - " + b.toString() + " ");
        }
        System.out.println(" ");
    }

    public void uniqueWords(String[] strings) {
        System.out.println("Уникальные слова:");
        ArrayList<String> string_list = new ArrayList<>();

        for (int i = 0; i < strings.length; i++) {
            String word = strings[i];
            if (!string_list.contains(word)) {
                string_list.add(word);
            }
        }
        for (String string : string_list) {
            System.out.print(string + " ");
        }
        System.out.println(" ");
    }

    public void sortList(String[] strings) {
        System.out.println("Отсортированные слова:");
        Set<String> words = new HashSet<>(Arrays.asList(strings));
        List sortedList = new ArrayList(words);
        Collections.sort(sortedList);


        for (Object string : sortedList) {
            System.out.println(string + " ");
        }
    }
}