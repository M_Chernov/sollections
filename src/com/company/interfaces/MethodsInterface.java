package com.company.interfaces;

public interface MethodsInterface {
    void numberOfRepeat(String[] strings);

    void uniqueWords(String[] strings);

    void sortList(String[] strings);
}
