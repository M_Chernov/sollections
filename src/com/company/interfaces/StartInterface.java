package com.company.interfaces;

import java.io.IOException;

public interface StartInterface {
    void reader() throws IOException;

    void splitString(String string);

    void start(String[] strings);
}
