package com.company;


import com.company.classes.Start;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        Start start = new Start();
        start.reader();
    }
}
